
export function createBAxios(config: import('axios').CreateAxiosDefaults | {
    offError?: boolean;
    codeErrMin?: number;
}, codoMap?: any): {
    (uri: string): BAxios;
    onErrMsg: (callback: (msg: string) => void) => void;
    onSuccMsg: (callback: (msg: string) => void) => void;
    onReqError: (callback: (err: import('axios').AxiosError) => void) => void;
    onResError: (callback: (err: import('axios').AxiosError) => void) => void;
};
/**
 * @class
 */
declare class BAxios {
    /**
     *
     * @param {import('axios').AxiosInstance} axios
     * @param { string } uri
     */
    constructor(axios: import('axios').AxiosInstance, uri: string);
    /**
     * get request
     * @param {import('axios').AxiosRequestConfig} config
     * @returns
     */
    get(config: import('axios').AxiosRequestConfig): Promise<import("axios").AxiosResponse<any, any>>;
    /**
     * post request
     * @param {any} data
     * @param {import('axios').AxiosRequestConfig} config
     * @returns
     */
    post(data: any, config: import('axios').AxiosRequestConfig): Promise<import("axios").AxiosResponse<any, any>>;
    /**
     *  post obj to FormData
     * @param {any} data
     * @param {import('axios').AxiosRequestConfig} config
     * @returns
     */
    postForm(data: any, config: import('axios').AxiosRequestConfig): Promise<import("axios").AxiosResponse<any, any>>;
    /**
     *
     * @param {*} data
     * @param {import('axios').AxiosRequestConfig} config
     */
    put(data: any, config: import('axios').AxiosRequestConfig): Promise<import("axios").AxiosResponse<any, any>>;
    /**
     *
     * @param {*} data
     * @param {import('axios').AxiosRequestConfig} config
     */
    putForm(data: any, config: import('axios').AxiosRequestConfig): Promise<import("axios").AxiosResponse<any, any>>;
    /**
     * @param {import('axios').AxiosRequestConfig} config
     */
    delete(config: import('axios').AxiosRequestConfig): Promise<import("axios").AxiosResponse<any, any>>;
    #private;
}
export {};

/**
 * 
 * @param { string } fName 
 * @param { string[] } [args=[]]
 * @param { string } ctx 
 * @returns 
 */
function createFun(fName, args = [], ctx) {
    let f = `function ${fName}(`
    args.forEach((d) => {
        f = f.concat(`${d},`)
        return
    })
    f = `${f.slice(0, f.length - 1)}){\n`
    return `${f}${ctx}\n}\n`
}


/**
 * @param { string[] } lines 
 */
function createBlock(lines){
    return lines.map((i)=>{
        return `    ${i}\n`
    })
}
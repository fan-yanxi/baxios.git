export type PostManCollectionItem = PostManRequest | PostManFolder

export type PostManRequest = {
    name: string;
    request: PRequest;
    response: PResponse[];
    description?: string;
}
export type PRequest = {
    method: "POST" | "GET" | "DELETE" | "PUT" | "post" | "get" | "delete" | "put";
    header: PHeader[];
    url: URL;
    description?: string;
    body?: ReqBody;
}
export type ReqBody = {
    mode: 'formdata' | 'urlencoded';
    formdata: PFormData[];
}
/**
 * 请求头
 */
export type PHeader = {
    key: string;
    value?: string;
    type?: string;
}
export type PBody = {}


export type PResponse<T> = {
    name: string;
    originalRequest: PRequest;
    _postman_previewlanguage: null;
    header: null | PHeader[];
    cookie: [];
    body: T;
}

export type URL = {
    raw: string;
    host: string[];
    path: string[];
    query: PQuery[];
}
export type PQuery = {
    key: string;
    value?: string;
    description?: string;
}
export type PostManFolder = {
    name: string;
    item?: PostManRequest[];
    description?: string;
}
export type PFormData = {
    key: string;
    value: string;
    type: "file" | "text";
}


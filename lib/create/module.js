
const TAB = '    '
/**
 * 
 * @param { string[] } modules 
 * @returns 
 */
function createExport(modules) {
    let content = 'export {'
    modules.forEach((d) => {
        content = content.concat(`\n${TAB}${d},`)
    })
    return `${content.slice(0, content.length - 1)}\n}`
}

/**
 * 
 * @param { string[] | string } modules
 * @param { string } from 
 * @returns 
 */
function createImport(modules, from) {
    if (Array.isArray(modules)) {
        let content = 'import {'
        modules.forEach((d) => {
            content = content.concat(` ${d} ,`)
        })
        return `${content.slice(0, content.length - 1)}} from '${from}'`
    } else {
        return `import ${modules} from '${from}'`
    }
}

/**
 * 创建一个创建函数代码
 * @param { string } fName 
 * @param { string[] } [args=[]]
 * @param { string } ctx 
 * @returns { string }
 */
function createFun(fName, args = [], ctx) {
    let f = `function ${fName}(`
    args.forEach((d) => {
        f = f.concat(`${d},`)
        return
    })
    f = `${f.slice(0, f.length - 1)}){\n`
    return `${f}${createBlock(ctx)}}\n`
}


/**
 * 创建缩进
 * @param { string[] | string } lines 
 * @param { number | string } [s=4] 
 * @returns { string }
 */
function createBlock(lines, s = 4) {
    if (!Array.isArray(lines)) {
        lines = lines.split("\n")
    }
    let sj = ""
    if (typeof s === "number") {
        s = " "
        for (let i = 0; i < 4; i++) {
            sj += s
        }
    }
    else {
        sj = s
    }
    return lines.map((i) => {
        return `${sj}${i}\n`
    }).join("")
}
/**
 * 创建json 代码块
 * @param { object } obj 
 * @returns { string[] }
 */
function createObj(obj) {
    return JSON.stringify(obj, null, 4).replaceAll("\"", "")
}

/**
 * 创建使用函数代码
 * @param { string } funcname 
 * @param { string []} param 
 * @returns { string }
 */
function createUseFun(funcname, param) {
    if (!Array.isArray(param)) {
        param = [param]
    }
    return `${funcname}(${param.join(",")})`
}

/**
 * 创建变量
 * @param { "var" | "const" | "let" } fun 
 * @param { string } name 
 * @param { string } val 
 * @returns { string }
 */
function createVar(fun, name, val) {
    return `${fun} ${name}${val ? ` = ${val}` : ""}`
}

/**
 * 创建一行一行的代码
 * @param { string[] } lines 
 * @returns { string }
 */
function createCode(lines) {
    return lines.map((i) => `${i}\n`).join("")
}

/**
 * 创建返回值
 * @param { string } r 
 * @returns 
 */
function createRetuen(r) {
    return `return ${r}`
}
export {
    createExport,
    createImport,
    createFun,
    createBlock,
    createObj,
    createUseFun,
    createCode,
    createVar,
    createRetuen
}

- [axios工具](#axios工具)
  - [介绍](#介绍)
  - [使用](#使用)
    - [下载](#下载)
    - [普通使用](#普通使用)
    - [代码生成器](#代码生成器)

# axios工具
## 介绍
> 这个库是一个对**axios**的二次封装,让请求能够预先定义好,需要发送的时候再使用对应的方法。更好的支持**RESTful**风格的请求,另外还支持根据自定义响应状态码映射表调用对应的方法。并且能够根据postman的导出生成对应的函数调用。
```ts
  //响应体必须为这种格式
  type Data = {
    data:any
    code:number
    msg:string
  } 
  
```

## 使用
### 下载
```sh
 npm i -g @b-hole/axios 
```
### 普通使用
> 普世使用
```js
import { createAxios } form 'baxios'
const config = {
  baseURL: location.origin + '/api/',
  withCredentials: true,
  timeout: 5000
}
// 响应状态码映射
const codeMap = {
  '200':"成功",
  '404':"没有查询到数据"
}
const request = axios.create(config)
const baxios = createBAxios({
  ...config
},codeMap)
// 创建请求 未发送 在这里设置完uri 路径过后
let test = baxios("uri")

/**
 * 另外baxios 上还可以绑定几个事件
 *  onErrMsg: (callback: (msg: string) => void) => void;
 *  onSuccMsg: (callback: (msg: string) => void) => void;
 *  onReqError: (callback: (err: import('axios').AxiosError) => void) => void;
 *  onResError: (callback: (err: import('axios').AxiosError) => void) => void;
 * **/
// 真实发送 这里的方法不能设置uri 路径
// 返回promise 且若是不报错的话 直接返回response 里的data
test.get()
test.post()
test.postForm()
```
### 代码生成器
> 首先你需要全局安装 **`@b-hole/axios`**
```sh
#生成axios函数文件
xxl p2a [postman导出的json文件地址] [输出文件地址]
#默认生成在命令执行目录下 api文件夹里
```
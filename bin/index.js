#!/usr/bin/env node --no-warnings
import { program } from "commander";
import pack from '../package.json' assert { type: "json" }
import { p2a } from "../lib/create/p2a.js";
program.version(pack.version)
program.name("xxl")
program.usage("<command> [options]")

/**
 * 定义p2a 命令
 */
const p2aCmd = program.command("p2a [filepath] [outpath]").description("把postman导出pai文档生成axios调用函数")

/**
 * 命令处理
 */
p2aCmd
    .usage("[options] [filepath] [outpath] 根据postman的导出json 生成axios 调用函数集合")
    .option("-b, --baxios", "根据postman的导出json 生成baxios 调用函数集合")
    .action((arg1, arg2, opts) => {
        if (Object.keys(opts).length === 0) {
            if (arg1) {
                p2a(arg1, arg2, 0)
            }
            else {
                p2aCmd.help()
            }
        } else {
            if (opts.baxios) {
                console.log("转换");
            }
        }
    })

program.parse(process.argv)


if (program.args.length === 0) {
    program.help()
}

